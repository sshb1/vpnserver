#!/bin/bash
#VPS Script By   : SSHBalap
#Contact Me FB   : https://www.facebook.com/SSHBalap

# Check Root
if [ "${EUID}" -ne 0 ]; then
echo "You need to run this script as root"
exit 1
fi

# Check System
if [ "$(systemd-detect-virt)" == "openvz" ]; then
echo "OpenVZ is not supported"
exit 1
fi

# Colours
red='\e[1;31m'
green='\e[0;32m'
NC='\e[0m'

# Requirement
apt update -y
apt upgrade -y
update-grub
apt install -y bzip2 gzip coreutils curl

# Disable ipv6
sysctl -w net.ipv6.conf.all.disable_ipv6=1 && sysctl -w net.ipv6.conf.default.disable_ipv6=1

# Script Access 
MYIP=$(wget -qO- icanhazip.com);
echo -e "${green}CHECKING SCRIPT ACCESS${NC}"
AUTH=$( curl https://gitlab.com/sshb1/vpnserver/-/raw/main/resources/users/auth | grep $MYIP )
if [ $MYIP = $AUTH ]; then
echo -e "${green}ACCESS GRANTED...${NC}"
else
echo -e "${green}ACCESS DENIED...${NC}"
exit 1
fi

#Subdomain Setting
mkdir /var/lib/premium-script;
echo "IP=$host" >> /var/lib/premium-script/ipvps.conf
wget https://gitlab.com/sshb1/vpnserver/-/raw/main/resources/services/config.sh && chmod +x config.sh && ./config.sh

# Install & download Script

# SSH OVPN
wget https://gitlab.com/sshb1/vpnserver/-/raw/main/resources/services/ssh-vpn.sh && chmod +x ssh-vpn.sh && screen -S ssh-vpn ./ssh-vpn.sh

# PPTP L2TP
wget https://gitlab.com/sshb1/vpnserver/-/raw/main/resources/services/pptp-l2tp.sh && chmod +x pptp-l2tp.sh && screen -S pptp-l2tp ./pptp-l2tp.sh

# SSTP
wget https://gitlab.com/sshb1/vpnserver/-/raw/main/resources/services/sstp.sh && chmod +x sstp.sh && screen -S sstp ./sstp.sh

# v2ray TROJAN
wget https://gitlab.com/sshb1/vpnserver/-/raw/main/resources/services/v2ray-tr.sh && chmod +x v2ray-tr.sh && screen -S v2ray ./v2ray-tr.sh

# TROJAN-GO
#wget https://gitlab.com/sshb1/vpnserver/-/raw/main/resources/services/trojan-go.sh && chmod +x trojan-go.sh && screen -S trojan-go ./trojan-go.sh

# WIREGUARD
wget https://gitlab.com/sshb1/vpnserver/-/raw/main/resources/services/wg.sh && chmod +x wg.sh && screen -S wg ./wg.sh

# SHADOWSOCK
wget https://gitlab.com/sshb1/vpnserver/-/raw/main/resources/services/ss.sh && chmod +x ss.sh && screen -S ss ./ss.sh

# SHADOWSOCK-R
wget https://gitlab.com/sshb1/vpnserver/-/raw/main/resources/services/ssr.sh && chmod +x ssr.sh && screen -S ssr ./ssr.sh

# PENGATURAN SYSTEM
wget https://gitlab.com/sshb1/vpnserver/-/raw/main/resources/services/set-br.sh && chmod +x set-br.sh && ./set-br.sh

# remove unnecessary files
cd
apt autoclean -y
apt -y remove --purge unscd
apt-get -y --purge remove samba*;
apt-get -y --purge remove apache2*;
apt-get -y --purge remove bind9*;
apt-get -y remove sendmail*
apt autoremove -y

# finishing
cd
chown -R www-data:www-data /home/vps/public_html
/etc/init.d/nginx restart
/etc/init.d/openvpn restart
/etc/init.d/cron restart
/etc/init.d/ssh restart
/etc/init.d/dropbear restart
/etc/init.d/fail2ban restart
/etc/init.d/stunnel4 restart
/etc/init.d/vnstat restart
/etc/init.d/squid restart
screen -dmS badvpn badvpn-udpgw --listen-addr 127.0.0.1:7100 --max-clients 500
screen -dmS badvpn badvpn-udpgw --listen-addr 127.0.0.1:7200 --max-clients 500
screen -dmS badvpn badvpn-udpgw --listen-addr 127.0.0.1:7300 --max-clients 500
screen -dmS badvpn badvpn-udpgw --listen-addr 127.0.0.1:7400 --max-clients 500
screen -dmS badvpn badvpn-udpgw --listen-addr 127.0.0.1:7500 --max-clients 500
screen -dmS badvpn badvpn-udpgw --listen-addr 127.0.0.1:7600 --max-clients 500
screen -dmS badvpn badvpn-udpgw --listen-addr 127.0.0.1:7700 --max-clients 500
screen -dmS badvpn badvpn-udpgw --listen-addr 127.0.0.1:7800 --max-clients 500
screen -dmS badvpn badvpn-udpgw --listen-addr 127.0.0.1:7900 --max-clients 500

history -c
echo "unset HISTFILE" >> /etc/profile
cd

clear
neofetch
netstat -nutlp

# Script Information
echo "1.2" > /home/ver
clear
echo " "
echo "Installation has been completed!!"
echo " "
echo "=================================-Autoscript Premium-===========================" | tee -a log-install.txt
echo "" | tee -a log-install.txt
echo "--------------------------------------------------------------------------------" | tee -a log-install.txt
echo ""  | tee -a log-install.txt
echo "   >>> Service & Port"  | tee -a log-install.txt
echo "   - OpenSSH                 : 22"  | tee -a log-install.txt
echo "   - OpenVPN                 : TCP 1194, UDP 2200, SSL 442"  | tee -a log-install.txt
echo "   - Stunnel4                : 443"  | tee -a log-install.txt
echo "   - Dropbear                : 442, 77"  | tee -a log-install.txt
echo "   - Squid Proxy             : 3128, 8080"  | tee -a log-install.txt
echo "   - PPTP                    : 1732"  | tee -a log-install.txt
echo "   - L2TP/IPSec              : 1701"  | tee -a log-install.txt
echo "   - SSTP                    : 444"  | tee -a log-install.txt
echo "   - V2RAY Vmess TLS         : 8443"  | tee -a log-install.txt
echo "   - V2RAY Vmess None TLS    : 80"  | tee -a log-install.txt
echo "   - V2RAY Vless TLS         : 2083"  | tee -a log-install.txt
echo "   - V2RAY Vless None TLS    : 8880"  | tee -a log-install.txt
echo "   - Trojan                  : 2087"  | tee -a log-install.txt
echo "   - Trojan-go               : 2087"  | tee -a log-install.txt
echo "   - Wireguard               : 7070"  | tee -a log-install.txt
echo "   - SS-OBFS TLS             : 2443-2543"  | tee -a log-install.txt
echo "   - SS-OBFS HTTP            : 3443-3543"  | tee -a log-install.txt
echo "   - Shadowsocks-R           : 1443-1543"  | tee -a log-install.txt
echo ""  | tee -a log-install.txt
echo "   >>> Server Information & Other Features"  | tee -a log-install.txt
echo "   - Timezone                : Asia/Makassar (GMT +8)"  | tee -a log-install.txt
echo "   - Fail2Ban                : [ON]"  | tee -a log-install.txt
echo "   - Dflate                  : [ON]"  | tee -a log-install.txt
echo "   - IPtables                : [ON]"  | tee -a log-install.txt
echo "   - Auto-Reboot             : [ON]"  | tee -a log-install.txt
echo "   - IPv6                    : [OFF]"  | tee -a log-install.txt
echo "   - Autoreboot On 00:00 GMT +8" | tee -a log-install.txt
echo "   - Autobackup Data" | tee -a log-install.txt
echo "   - Restore Data" | tee -a log-install.txt
echo "   - Auto Delete Expired Account" | tee -a log-install.txt
echo "   - Full Orders For Various Services" | tee -a log-install.txt
echo "   - White Label" | tee -a log-install.txt
echo "   - Installation Log --> /root/log-install.txt"  | tee -a log-install.txt
echo ""  | tee -a log-install.txt
echo "   - Dev/Main                : Horas Marolop Amsal Siregar"  | tee -a log-install.txt
echo "   - Telegram                : T.me/SSHBalap"  | tee -a log-install.txt
echo "------------------Script ReMod By SSHBalap-----------------" | tee -a log-install.txt
echo ""
rm -f setup.sh
