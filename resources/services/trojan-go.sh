#!/bin/bash

if [ -f /etc/centos-release ]; then
	yum install -y wget curl zip
else
	apt install -y wget curl zip
fi
mkdir -p /etc/trojan-go
touch /etc/trojan-go/akun.conf
mkdir /usr/lib/trojan-go
wget -N --no-check-certificate https://github.com/p4gefau1t/trojan-go/releases/download/$(curl -fsSL https://api.github.com/repos/p4gefau1t/trojan-go/releases | grep '"tag_name":' | head -n 1 | sed -E 's/.*"([^"]+)".*/\1/')/trojan-go-linux-amd64.zip && unzip -d /usr/lib/trojan-go/ ./trojan-go-linux-amd64.zip && mv /usr/lib/trojan-go/trojan-go /usr/bin/ && chmod +x /usr/bin/trojan-go && rm -rf ./trojan-go-linux-amd64.zip
cp /usr/lib/trojan-go/example/server.json /etc/trojan-go/config.json
cp /usr/lib/trojan-go/example/trojan-go.service /etc/systemd/system/trojan-go.service
systemctl daemon-reload
systemctl enable trojan-go
echo Done!

# Tambahan
cd /usr/bin
wget -O add-trgo "https://gitlab.com/sshb1/vpnserver/-/raw/main/resources/menu/trojango/add-trgo.sh"
wget -O del-trgo "https://gitlab.com/sshb1/vpnserver/-/raw/main/resources/menu/trojango/del-trgo.sh"
wget -O cek-trgo "https://gitlab.com/sshb1/vpnserver/-/raw/main/resources/menu/trojango/cek-trgo.sh"
wget -O renew-trgo "https://gitlab.com/sshb1/vpnserver/-/raw/main/resources/menu/trojango/renew-trgo.sh"
chmod +x add-trgo.sh
chmod +x del-trgo.sh
chmod +x cek-wg
chmod +x renew-trgo.sh
cd
rm -f /root/trojan-go.sh